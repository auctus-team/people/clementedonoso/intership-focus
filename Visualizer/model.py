from opensim import *
import numpy as np

model = Model()

G: Body = model.getGround()

B1 = Body()
B1.setName("Arm")
B1.setMass(1.0)
B1.setMassCenter(Vec3(0,0,0))
B1.setInertia(Inertia(0,0,0))

B2 = Body()
B2.setName("Forearm")
B2.setMass(1.0)
B2.setMassCenter(Vec3(0,0,0))
B2.setInertia(Inertia(0,0,0))

B1_length = 4
# ground here is directly at joint1

# shoulder rotation y axis and z axis. We have to lock the x axis
J2 = BallJoint("Shoulder", G, Vec3(0,0,0), Vec3(0,0,0), B1, Vec3(0,0,0), Vec3(0,0,0))
# elbow rotation z axis
J3 = PinJoint("Elbow", B1, Vec3(B1_length,0,0), Vec3(0,0,0), B2, Vec3(0,0,0), Vec3(0,0,0))

C1 = PathActuator()
C1.setName("shoulder-pronation")
C1.addNewPathPoint("origin", G, Vec3(0,0,2))
C1.addNewPathPoint("insertion", B1, Vec3(2,0,1))

C2 = PathActuator()
C2.setName("shoulder-supination")
C2.addNewPathPoint("origin", G, Vec3(0,0,-2))
C2.addNewPathPoint("insertion", B1, Vec3(2,0,-1))

C3 = PathActuator()
C3.setName("shoulder-flexor")
C3.addNewPathPoint("origin", B1, Vec3(3,2,0))
C3.addNewPathPoint("insertion", B2, Vec3(1,2,0))

C4 = PathActuator()
C4.setName("shoulder-extensor")
C4.addNewPathPoint("origin", B1, Vec3(3,-2,0))
C4.addNewPathPoint("insertion", B2, Vec3(1,-2,0))

# C1 and C2 are symetric an xz-plane, C3 and C4 on xy-plane

C5 = PathActuator() # xy-plane
C5.setName("elbow-flexor")
C5.addNewPathPoint("origin", G, Vec3(0,2,0))
C5.addNewPathPoint("insertion", B1, Vec3(2,1,0))
C6 = PathActuator() # symmetric on xy-plane
C6.setName("elbow-extensor")
C6.addNewPathPoint("origin", G, Vec3(0,-2,0))
C6.addNewPathPoint("insertion", B1, Vec3(2,-1,0))


model.setGravity(Vec3(0,0,0))
model.addBody(B1)
model.addBody(B2)
# model.addJoint(J1)
model.addJoint(J2)
model.addJoint(J3)
model.addForce(C1)
model.addForce(C2)
model.addForce(C3)
model.addForce(C4)
model.addForce(C5)
model.addForce(C6)

pathActuators = np.array([C1, C2, C3, C4, C5, C6])

state: State = model.initSystem()

subsystem: SimbodyMatterSubsystem = model.getMatterSubsystem()
# COORDINATE INITIALIZATION (in the state) -----------------------------
q_null = J2.get_coordinates(0) # we dont care about this one but we didn't really have a choice to have it
q_null.setLocked(state, True)
q_null.setName("Shoulder adduction-abduction")
q1 = J2.get_coordinates(1) # y-axis so shoulder pronation supination
q1.setName("Shoulder pronation-supination")
q1.setRangeMin(-np.pi/2)
q1.setRangeMax(np.pi/2)

q2 = J2.get_coordinates(2) # z-axis so shoulder flexion extension
q2.setName("Shoulder flexion-extension")
q2.setRangeMin(-np.pi/2)
q2.setRangeMax(np.pi/2)

q3 = J3.getCoordinate() # x axis so elbow flexion extension
q3.setName("Elbow flexion-extension")
q3.setRangeMin(0)
q3.setRangeMax(np.pi)

endEffector = Vec3(1,0,0)


def computeJacobianTranspose():
    model.equilibrateMuscles(state)

    J = Matrix()
    subsystem.calcFrameJacobian(state, B2.getMobilizedBodyIndex(), endEffector, J)
    JLinearPart = J.to_numpy()[3:6,[1,2,3]] # We don't take J1_q0 (x axis) because we don't need it
    # print("\nJ = ")
    # print(np.round(JLinearPart,4))
    JLinearPartTranspose = JLinearPart.T
    JTPretty = np.round(JLinearPartTranspose,4)
    JTPretty += 0
    print("\nJ^T = ")
    print(JTPretty)

    return JLinearPartTranspose

def calcMomentArmMatrix(model: Model, state: State, pathActuators):
    model.equilibrateMuscles(state) 

    nbDoF = model.getCoordinateSet().getSize()

    nbPathActuators = pathActuators.size
    N = np.zeros((nbDoF, nbPathActuators)) 

    for i in range(nbDoF):
        coord = model.getCoordinateSet().get(i)
        # print(coord.getName()) # if you need to see the rows order

        for j in range(nbPathActuators):
            pathActuator: PathActuator = pathActuators[j]            
            N[i,j] = pathActuator.computeMomentArm(state, coord)
    return N

