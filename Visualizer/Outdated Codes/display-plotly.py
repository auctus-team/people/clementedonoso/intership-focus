from dash import Dash, html, dcc, Input, Output
import plotly.graph_objects as go
import plotly.express as px
import plotly.figure_factory as ff
import numpy as np
from plotly.subplots import make_subplots
from opensim import *

# MODEL PART ---------------------
# --------------------------------
# --------------------------------

model = Model()

G: Body = model.getGround()

B1 = Body()
B1.setName("B1")
B1.setMass(1.0)
B1.setMassCenter(Vec3(0,0,0))
B1.setInertia(Inertia(0,0,0))

B2 = Body()
B2.setName("B2")
B2.setMass(1.0)
B2.setMassCenter(Vec3(0,0,0))
B2.setInertia(Inertia(0,0,0))

B1_length = 4
# ground here is directly at joint1

# shoulder rotation y axis and z axis. We have to lock the x axis
J2 = BallJoint("J2", G, Vec3(0,0,0), Vec3(0,0,0), B1, Vec3(0,0,0), Vec3(0,0,0))
# elbow rotation z axis
J3 = PinJoint("J3", B1, Vec3(B1_length,0,0), Vec3(0,0,0), B2, Vec3(0,0,0), Vec3(0,0,0))

C1 = PathActuator()
C1.setName("C1")
C1.addNewPathPoint("origin", G, Vec3(0,0,2))
C1.addNewPathPoint("insertion", B1, Vec3(2,0,1))

C2 = PathActuator()
C2.setName("C2")
C2.addNewPathPoint("origin", G, Vec3(0,0,-2))
C2.addNewPathPoint("insertion", B1, Vec3(2,0,-1))

C3 = PathActuator()
C3.setName("C3")
C3.addNewPathPoint("origin", B1, Vec3(3,2,0))
C3.addNewPathPoint("insertion", B2, Vec3(1,2,0))

C4 = PathActuator()
C4.setName("C4")
C4.addNewPathPoint("origin", B1, Vec3(3,-2,0))
C4.addNewPathPoint("insertion", B2, Vec3(1,-2,0))

# C1 and C2 are symetric an xz-plane, C3 and C4 on xy-plane

C5 = PathActuator() # xy-plane
C5.setName("C5")
C5.addNewPathPoint("origin", G, Vec3(0,2,0))
C5.addNewPathPoint("insertion", B1, Vec3(2,1,0))
C6 = PathActuator() # symmetric on xy-plane
C6.setName("C6")
C6.addNewPathPoint("origin", G, Vec3(0,-2,0))
C6.addNewPathPoint("insertion", B1, Vec3(2,-1,0))


model.setGravity(Vec3(0,0,0))
model.addBody(B1)
model.addBody(B2)
# model.addJoint(J1)
model.addJoint(J2)
model.addJoint(J3)
model.addForce(C1)
model.addForce(C2)
model.addForce(C3)
model.addForce(C4)
model.addForce(C5)
model.addForce(C6)

pathActuators = np.array([C1, C2, C3, C4, C5, C6])

state: State = model.initSystem()

subsystem: SimbodyMatterSubsystem = model.getMatterSubsystem()
# COORDINATE INITIALIZATION (in the state) -----------------------------
q1 = J2.get_coordinates(1) # y-axis so shoulder pronation supination
q2 = J2.get_coordinates(2) # z-axis so shoulder flexion extension
q_null = J2.get_coordinates(0) # we dont care about this one but we didn't really have a choice to have it
q_null.setLocked(state, True)
q3 = J3.getCoordinate() # x axis so elbow flexion extension

endEffector = Vec3(1,0,0)


def computeJacobianTranspose():
    model.equilibrateMuscles(state)

    J = Matrix()
    subsystem.calcFrameJacobian(state, B2.getMobilizedBodyIndex(), endEffector, J)
    JLinearPart = J.to_numpy()[3:6,[1,2,3]] # We don't take J1_q0 (x axis) because we don't need it
    # print("\nJ = ")
    # print(np.round(JLinearPart,4))
    JLinearPartTranspose = JLinearPart.T
    JTPretty = np.round(JLinearPartTranspose,4)
    JTPretty += 0
    print("\nJ^T = ")
    print(JTPretty)

    return JLinearPartTranspose

def calcMomentArmMatrix(model: Model, state: State, pathActuators):
    model.equilibrateMuscles(state) 

    nbDoF = model.getCoordinateSet().getSize()

    nbPathActuators = pathActuators.size
    N = np.zeros((nbDoF, nbPathActuators)) 

    for i in range(nbDoF):
        coord = model.getCoordinateSet().get(i)
        # print(coord.getName()) # if you need to see the rows order

        for j in range(nbPathActuators):
            pathActuator: PathActuator = pathActuators[j]            
            N[i,j] = pathActuator.computeMomentArm(state, coord)
    return N

# APP PART -----------------------
# --------------------------------
# --------------------------------
app = Dash(__name__)

app.layout = html.Div([
    html.Div(children=[
        html.Div(children=[
            html.H2('Display', 
                style={
                    'margin': 0, 
                    'background-color': '#f3f3f3',
                    'padding': '10px',
                    'border-radius': '5px 5px 0 0'
                }),

            html.Div(children=[
                dcc.Checklist(['Im(J^T)'],
                            ['Im(J^T)']
                ),        
                dcc.Checklist(['Nt'],
                            ['Nt']),
                html.Hr(),
                dcc.Checklist(['Basis of Im(J^T)'],
                            []),
                dcc.Checklist(['Generators of Nt'],
                            ['Generators of Nt']),
                dcc.Checklist(['Origin'],
                            ['Origin']),
            ], style={'padding': '10px'})
        ], style={
            'border': '1px solid lightgray', 
            'margin-bottom': '10px',
            'border-radius': '5px',
            'box-shadow': '0 2px 5px #f3f3f3'
            }),
        
        # --- ARM CONFIGURATION
        html.Div(children=[
            html.H2('Bodies', 
                style={
                    'margin': 0, 
                    'background-color': '#f3f3f3',
                    'padding': '10px',
                    'border-radius': '5px 5px 0 0'
                }),

            html.Div(children=[
                html.Label('Arm length'),
                dcc.Slider(
                    id='armLength-slider',
                    min=0,
                    max=1,
                    marks={
                        0: '0m',
                        1: '1m'
                    },
                    tooltip={"placement": "bottom", "always_visible": True},
                    value=0.3
                ),

                html.Br(),

                html.Label('Forearm length'),
                dcc.Slider(
                    id='forearmLength-slider',
                    min=0,
                    max=1,
                    marks={
                        0: '0m',
                        1: '1m'
                    },
                    tooltip={"placement": "bottom", "always_visible": True},
                    value=0.4
                ),
            ], style={'padding': '10px'})
        ], style={
            'border': '1px solid lightgray', 
            'margin-bottom': '10px',
            'border-radius': '5px',
            'box-shadow': '0 2px 5px #f3f3f3'
            }),
        
        html.Div(children=[
            html.H2('Joints', 
                style={
                    'margin': 0, 
                    'background-color': '#f3f3f3',
                    'padding': '10px',
                    'border-radius': '5px 5px 0 0'
                }),

            html.Div(children=[
                html.Label('Shoulder pronation-supination'),
                dcc.Slider(
                    id='sPS-slider',
                    min=-90,
                    max=90,
                    # marks={i: str(i) for i in range(1, 90)},
                    marks={
                        -90: '-90°',
                        0: '0°',
                        90: '90°'
                    },
                    tooltip={"placement": "bottom", "always_visible": True},
                    value=0
                ),

                html.Br(),

                html.Label('Shoulder flexion-extension'),
                dcc.Slider(
                    id='sFE-slider',
                    min=-90,
                    max=90,
                    # marks={i: str(i) for i in range(1, 90)},
                    marks={
                        -90: '-90°',
                        0: '0°',
                        90: '90°'
                    },
                    tooltip={"placement": "bottom", "always_visible": True},
                    value=0
                ),

                html.Br(),

                html.Label('Elbow flexion-extension'),
                dcc.Slider(
                    id='eFE-slider',
                    min=0,
                    max=180,
                    # marks={i: str(i) for i in range(1, 90)},
                    marks={
                        0: '0°',
                        90: '90°',
                        180: '180°'
                    },
                    tooltip={"placement": "bottom", "always_visible": True},
                    value=0
                ),
                

            ], style={'padding': '10px'})
        ], style={'border': '1px solid lightgray',
                    'border-radius': '5px',
                    'margin-bottom': '10px',
                    'box-shadow': '0 2px 5px #f3f3f3'
                    }),

        # --- CABLE CONFIGURATION
        html.Div(children=[
            html.H2('Cables', 
                style={
                    'margin': 0, 
                    'background-color': '#f3f3f3',
                    'padding': '10px',
                    'border-radius': '5px 5px 0 0'
                }),

            html.P('-Manipulate the t vector here'),
            html.P('-Manipulate the cable origins and insertions')
        ], style={
            'border': '1px solid lightgray', 
            'margin-bottom': '10px',
            'border-radius': '5px',
            'box-shadow': '0 2px 5px #f3f3f3'
            }),

        
    ], style={
        'display': 'flex', 
        'flex-direction': 'column',
        'overflow-y': 'auto',
        # 'height': '100vh',
        # 'padding': '10px'
    }),

    
    html.Div(children=[
        dcc.Graph(id='graphTest', mathjax=True),
    ], style={'flex': 1, 'display': 'flex', 'border': '0px solid green',
        'justify-content': 'center'
        })
], style={
        'display': 'flex', 
        'flex-direction': 'row',
        'font-family': 'Arial',
        'height': '100%',
        'width': '100%',
        'border': '0px solid red'
        })



def getFigJacobianTranspose():
    Jt = computeJacobianTranspose()

    # Values to construct the Jacobian Plane - For now just numbers
    p1 = Jt[:,0]
    p2 = Jt[:,1]
    p3 = Jt[:,2]

    v1 = p3 - p1
    v2 = p2 - p1

    n = np.cross(v1, v2)
    a, b, c = n
    d = np.dot(n, p3)
    print('The equation of J1 is {0}x + {1}y + {2}z = {3}'.format(a, b, c, d))

    X, Y = np.meshgrid(range(-20,20), range(-20,20))
    Z = (d - a * X - b * Y) / c

    figJt = go.Surface(z=Z, x=X, y=Y, opacity=.8)

    fig = go.Figure()
    # figJt.colorscale=[[1, 'blue']]
    fig.add_trace(figJt)

    fig.add_scatter3d(
        x=[0], 
        y=[0], 
        z=[0], 
        mode="markers",
        marker=dict(
            size=2,          
            color="rgb(0,0,0)"))

    camera = dict(
        up=dict(x=0, y=1, z=0),
        center=dict(x=0, y=0, z=0),
        eye=dict(x=2, y=0.5, z=2)
    )

    fig.update_layout(
        title=r'$\mathcal{P}_{\tau} \text{ and } Im(J^T)$',
        legend_title="Legend Title",
        scene_camera=camera,
        scene = dict(
            xaxis_title='x',
            yaxis_title='y',
            zaxis_title='z'),
        scene_aspectmode='cube'
    )

    return fig

@app.callback(
    Output('graphTest', 'extendData'),
    Input('sPS-slider', 'value'),
    Input('sFE-slider', 'value'),
    Input('eFE-slider', 'value'),
    Input('armLength-slider', 'value'),
    Input('forearmLength-slider', 'value')
)
def update_q(sPS, sFE, eFE, armLength, forearmLength):
    q1.setValue(state, sPS)
    q2.setValue(state, sFE)
    q3.setValue(state, eFE)

    fig = getFigJacobianTranspose()
    return fig
    # return f'Output: {sPS}, {sFE}, {eFE}'


if __name__ == '__main__':
    app.run_server(debug=True)
