from operator import le
from matplotlib import projections
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
from matplotlib.widgets import Slider, Button

# Configuration of the plot
fig = plt.figure()

plot3D = fig.add_subplot(111, projection='3d')
plot3D.view_init(azim=30, elev=20, vertical_axis="y")
plot3D.set_xlabel('X')
plot3D.set_ylabel('Y')
plot3D.set_zlabel('Z')

plot3D.set_xlim(-20,20)
plot3D.set_ylim(-20,20)
plot3D.set_zlim(-20,20)
plt.subplots_adjust(bottom=0.30)

tMax = np.array([10, 20, 15, 12, 4, 6])
# T-polytope = N @ t

N = np.array([
    [1, 10, 0, 0, 0, 0],
    [0, 0, 1, 1, 0, 0],
    [0, 0, 0, 0, 1 ,1]
])

tau = np.dot(N, tMax)
print('tau vector is {a}'.format(a=tau))

# JACOBIAN 1 ---------------------------
Jt = np.array([
    [2, 1, 2],
    [1, 0, 0],
    [0, 0, 0],
])

# Values to construct the Jacobian Plane - For now just numbers
p1 = Jt[:,0]
p2 = Jt[:,1]
p3 = Jt[:,2]

v1 = p3 - p1
v2 = p2 - p1

n = np.cross(v1, v2)
a, b, c = n
d = np.dot(n, p3)
print('The equation of J1 is {0}x + {1}y + {2}z = {3}'.format(a, b, c, d))

# Width and height of the Jacobian Plane1
X, Y = np.meshgrid(range(-20,20), range(-20,20))
Z = (d - a * X - b * Y) / c

jacobian = plot3D.plot_surface(X, Y, Z, alpha=0.2, color='blue')

# Printing the Jt basis
plot3D.text(0, 0, 0, 'J1', color='black', fontsize=15)
plot3D.quiver(0, 0, 0, p1[0], p1[1], p1[2], color="r")
plot3D.quiver(0, 0, 0, p2[0], p2[1], p2[2], color="g")
plot3D.quiver(0, 0, 0, a, b, c, color="b")
# -------------------------------------------------

# JACOBIAN 2 ---------------------------
Jt1 = np.array([
    [3, 2, 1],
    [1, -1, 0],
    [5, 5, 5],
])

# Values to construct the Jacobian Plane - For now just numbers
p1 = Jt1[:,0]
p2 = Jt1[:,1]
p3 = Jt1[:,2]

v1 = p3 - p1
v2 = p2 - p1

n = np.cross(v1, v2)
a, b, c = n
d = np.dot(n, p3)
print('The equation of J2 is {0}x + {1}y + {2}z = {3}'.format(a, b, c, d))

# Width and height of the Jacobian Plane2 <---------- ISSUE HERE!!
X, Y = np.meshgrid(range(-20,20), range(-20,20))
Z = (d - a * X - b * Y) / c

jacobian = plot3D.plot_surface(X, Y, Z, alpha=0.2, color='green')

# Printing the Jt basis
plot3D.text(0, 5, 0, 'J2', color='black', fontsize=15)
plot3D.quiver(0, 5, 0, p1[0], p1[1], p1[2], color="r")
plot3D.quiver(0, 5, 0, p2[0], p2[1], p2[2], color="g")
plot3D.quiver(0, 5, 0, a, b, c, color="b")
# -------------------------------------------------

# Values to construct the torque polytope (from vertices) - For now just numbers

xN, yN, zN = [0,0,5] # just to see it differently from the origin of Jt, but should be 0,0,0
plot3D.text(xN, yN, zN, 'P', color='black', fontsize=15)


plot3D.quiver(xN, yN, zN, N[0,0], N[1,0], N[2,0], color="r")
plot3D.quiver(xN, yN, zN, -N[0,1], -N[1,1], -N[2,1], color="r")
plot3D.quiver(xN, yN, zN, N[0,2], N[1,2], N[2,2], color="g")
plot3D.quiver(xN, yN, zN, -N[0,3], -N[1,3], -N[2,3], color="g")
plot3D.quiver(xN, yN, zN, N[0,4], N[1,4], N[2,4], color="b")
plot3D.quiver(xN, yN, zN, -N[0,5], -N[1,5], -N[2,5], color="b")

points = np.array([
    [-1, -2, -1],
    [1, -2, -1 ],
    [1, 2, -1],
    [-1, 2, -1],
    [-1, -2, 1],
    [1, -2, 1 ],
    [1, 2, 1],
    [-1, 2, 1]])

planeZ = points
planeZ = 5.0*planeZ

verts = [[planeZ[0],planeZ[1],planeZ[2],planeZ[3]],
 [planeZ[4],planeZ[5],planeZ[6],planeZ[7]],
 [planeZ[0],planeZ[1],planeZ[5],planeZ[4]],
 [planeZ[2],planeZ[3],planeZ[7],planeZ[6]],
 [planeZ[1],planeZ[2],planeZ[6],planeZ[5]],
 [planeZ[4],planeZ[7],planeZ[3],planeZ[0]]]
polytope = plot3D.add_collection3d(Poly3DCollection(verts, facecolors='red', linewidths=0.1, edgecolors='black', alpha=.1))

# Values for the sliders - display position - layout - min value - max value - init value
q1_slider = Slider(plt.axes([0.1, 0.1, 0.65, 0.03]), 'q1', 0.0, 10.0, valinit=1)
q2_slider = Slider(plt.axes([0.1, 0.05, 0.65, 0.03]), 'q2', 0.0, 10.0, valinit=1)

# Add a reset button
def resetButton(event):
    q1_slider.reset()
    q2_slider.reset()

resetax = plt.axes([0.87, 0.9, 0.1, 0.04])
button = Button(resetax, 'reset', color='gold', hovercolor='0.975')

'''
# Add button to change the axis rotation
axis = 'x'
def xButton(event):
    global X, Y, Z, jacobian
    axis = 'x'
    X,Y,Z = rotate(90, axis, X, Y, Z)
    jacobian.remove()
    plot3D.set_xlabel('X')
    plot3D.set_ylabel('Y')
    plot3D.set_zlabel('Z')
    jacobian = plot3D.plot_surface(X, Y, Z, alpha=0.8, color='blue')

def yButton(event):
    global X, Y, Z, jacobian
    axis = 'y'
    X,Y,Z = rotate(90, axis, X, Y, Z)
    jacobian.remove()
    plot3D.set_xlabel('X')
    plot3D.set_ylabel('Y')
    plot3D.set_zlabel('Z')
    jacobian = plot3D.plot_surface(X, Y, Z, alpha=0.8, color='blue')

def zButton(event):
    global X, Y, Z, jacobian
    axis = 'z'
    X,Y,Z = rotate(90, axis, X, Y, Z)
    jacobian.remove()
    plot3D.set_xlabel('X')
    plot3D.set_ylabel('Y')
    plot3D.set_zlabel('Z')
    jacobian = plot3D.plot_surface(X, Y, Z, alpha=0.8, color='blue')

# Position of the buttons
buttonx = plt.axes([0.87, 0.05, 0.1, 0.04])
buttony = plt.axes([0.87, 0.1, 0.1, 0.04])
buttonz = plt.axes([0.87, 0.15, 0.1, 0.04])
buttonx = Button(buttonx, 'x', color='orange', hovercolor='0.975')
buttony = Button(buttony, 'y', color='orange', hovercolor='0.975')
buttonz = Button(buttonz, 'z', color='orange', hovercolor='0.975')


# Rotate the Jacobian plane
def rotate(angle, axis, X, Y, Z):
    newPlane = np.transpose(np.array([X,Y,Z]), (1,2,0))
    if axis == 'x':
        m = [[0, np.cos(angle), -np.sin(angle)] , [0, np.sin(angle), np.cos(angle)], [0, 0, 0]]
    elif axis == 'y':
        m = [[np.cos(angle), 0, np.sin(angle)], [0, 1, 0], [-np.sin(angle), 0, np.cos(angle)]]
    elif axis == 'z':
        m = [[np.cos(angle), -np.sin(angle), 0], [np.sin(angle), np.cos(angle), 0], [0, 0, 1]]
    X, Y, Z = np.transpose(np.dot(newPlane, m), (2,0,1))

    return X, Y, Z

'''

# Update de plot3D when the sliders are moved
def update(val): 
    plot3D.clear()

    value1 = q1_slider.val
    value2 = q2_slider.val

    plot3D.set_xlabel('X')
    plot3D.set_ylabel('Y')
    plot3D.set_zlabel('Z')

    plot3D.set_xlim(-20,20)
    plot3D.set_ylim(-20,20)
    plot3D.set_zlim(-20,20)
    plt.subplots_adjust(bottom=0.30)

    tMax = np.array([10, 20, 15, 12, 4, 6])
    # T-polytope = N @ t

    N = np.array([
        [1, 10, 0, 0, 0, 0],
        [0, 0, 1, 1, 0, 0],
        [0, 0, 0, 0, 1 ,1]
    ])

    tau = np.dot(N, tMax)

    # JACOBIAN 1 ---------------------------
    Jt = np.array([
        [2, 1, 2],
        [1, 0, 0],
        [0, 0, 0],
    ])

    # Values to construct the Jacobian Plane - For now just numbers
    p1 = Jt[:,0]
    p2 = Jt[:,1]
    p3 = Jt[:,2]

    v1 = p3 - p1
    v2 = p2 - p1

    n = np.cross(v1, v2)
    a, b, c = n
    d = np.dot(n, p3)

    # Width and height of the Jacobian Plane1
    X, Y = np.meshgrid(range(-20,20), range(-20,20))
    Z = (d - a * X - b * Y) / c

    jacobian = plot3D.plot_surface(X, Y, Z, alpha=0.2, color='blue')

    # Printing the Jt basis
    plot3D.text(0, 0, 0, 'J1', color='black', fontsize=15)
    plot3D.quiver(0, 0, 0, p1[0], p1[1], p1[2], color="r")
    plot3D.quiver(0, 0, 0, p2[0], p2[1], p2[2], color="g")
    plot3D.quiver(0, 0, 0, a, b, c, color="b")
    # -------------------------------------------------

    # Values to construct the torque polytope (from vertices) - For now just numbers

    xN, yN, zN = [0,0,5] # just to see it differently from the origin of Jt, but should be 0,0,0
    plot3D.text(xN, yN, zN, 'P', color='black', fontsize=15)


    plot3D.quiver(xN, yN, zN, N[0,0], N[1,0], N[2,0], color="r")
    plot3D.quiver(xN, yN, zN, -N[0,1], -N[1,1], -N[2,1], color="r")
    plot3D.quiver(xN, yN, zN, N[0,2], N[1,2], N[2,2], color="g")
    plot3D.quiver(xN, yN, zN, -N[0,3], -N[1,3], -N[2,3], color="g")
    plot3D.quiver(xN, yN, zN, N[0,4], N[1,4], N[2,4], color="b")
    plot3D.quiver(xN, yN, zN, -N[0,5], -N[1,5], -N[2,5], color="b")

    points = np.array([
        [-1, -2*value1, -1*value2],
        [1, -2*value1, -1*value2 ],
        [1, 2*value1, -1*value2],
        [-1, 2*value1, -1*value2],
        [-1, -2*value1, 1*value2],
        [1, -2*value1, 1*value2 ],
        [1, 2*value1, 1*value2],
        [-1, 2*value1, 1*value2]])

    planeZ = points
    planeZ = 5.0*planeZ

    verts = [[planeZ[0],planeZ[1],planeZ[2],planeZ[3]],
    [planeZ[4],planeZ[5],planeZ[6],planeZ[7]],
    [planeZ[0],planeZ[1],planeZ[5],planeZ[4]],
    [planeZ[2],planeZ[3],planeZ[7],planeZ[6]],
    [planeZ[1],planeZ[2],planeZ[6],planeZ[5]],
    [planeZ[4],planeZ[7],planeZ[3],planeZ[0]]]
    plot3D.add_collection3d(Poly3DCollection(verts, facecolors='red', linewidths=0.1, edgecolors='black', alpha=.1))

    
    

# Connect the auxiliary function to the widgets
q1_slider.on_changed(update)
q2_slider.on_changed(update)
button.on_clicked(resetButton)
'''
buttonx.on_clicked(xButton)
buttony.on_clicked(yButton)
buttonz.on_clicked(zButton)
'''
plt.show()
# plt.savefig("n")