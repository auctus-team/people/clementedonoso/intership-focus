# Visualizer for Gautier Laisné

## About the project

For run the code, you just need to run the ``` app.py ```

## J plane behavior

I will use the following acronyms to make it easier to explain:
- **SPS**: Shoulder pronation-supination
- **SFE**: Shoulder flexion-extension
- **EFE**: Elbow felxion-extension

- - -
### Some important things that I see

- Behavior when SPS, SFE or EFE ar at the extremes values.
    - When de SPS is at the extremes, the Z axis explodes (Z = 1*10<sup>17</sup>) or the plane just disappear
    - When SFE and EFE are both in the extremes, the Z axis explodes (Z = 1*10<sup>17</sup>)
    - When only SFE is at the extremes, nothing seems to happen.

- In certain combinations of SPS, SFE and EFE the plane abruptly changes position. *Why is this happening, is it normal?*

- Symmetries seem to exist, but under specific conditions or combinations of values ​​(SPS, SFE, and EFE), I thought the symmetry could be more generic.
    - Why does the "turn" end up being faster and isn't always "identical" as the slider moves?
    - Visually, an equality close to the extremes is observed, but not beyond those values.
    - When SFE = 90° and EFE=180°, there is a symmetry for positive and negative SPS values
    - When SFE = 0° and EFE=0°, there is a symmetry for positive and negative SPS values
    - Could it be that EFE indicates the "axis of symmetry" that SFE will have? *Since if I change the value of EFE, the symmetry of SFE shifts*

- Interesting "turnovers" happen, as if it were "wobbly" from the beginning
    - There is also a rotation pattern (if a value grows it "turns" at all times in the same direction)

- When EFE = 0° and SPS = 0°, SFE not change the plot any time, except when it´s on -90° and it changes abruptly. *Why is this happening?*

- When EFE = 180° and SPS = 0°, SFE not change the plot any time, except when it´s on -90° and 90°, and it changes abruptly. *Why is this happening?*