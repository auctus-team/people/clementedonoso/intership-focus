from turtle import update
from dash import html, dcc, ctx
from dash_extensions.enrich import Output, DashProxy, Input, MultiplexerTransform, ALL
from dash.exceptions import PreventUpdate
import plotly.graph_objects as go
import plotly.express as px
import plotly.figure_factory as ff
import numpy as np
import matplotlib.pyplot as plt
from plotly.subplots import make_subplots
from plotly import colors
import model as osimModel
model = osimModel.model
state = osimModel.state

# APP PART -----------------------
# --------------------------------
# --------------------------------
# app = Dash(__name__, title='Bioctus Visualizer')
app = DashProxy(
    # suppress_callback_exceptions=True,
    #  prevent_initial_callbacks=True, 
     transforms=[MultiplexerTransform()])

header = html.Div(children=[
    html.H1("FFS Visualizer")
], className="header")

databar = html.Div(children=[
    html.Div(children=[
    html.Div(children=[
        html.Div(children=[
            html.Label('Arm length'),
            dcc.Slider(
                id='armLenssgth-slider',
                min=0,
                max=1,
                marks={
                    0: '0m',
                    1: '1m'
                },
                tooltip={"placement": "bottom", "always_visible": True},
                value=0.3,
                updatemode='drag' #Using updatemode = 'drag' you will update de plot always
            ),

            html.Br(),

            html.Label('Foredsdarm length'),
            dcc.Slider(
                id='forearmLsenagth-slider',
                min=0,
                max=1,
                marks={
                    0: '0m',
                    1: '1m'
                },
                tooltip={"placement": "bottom", "always_visible": True},
                value=0.4,
                updatemode='drag' #Using updatemode = 'drag' you will update de plot always
            ),
        ], className="parameters")
    ], className="configurationBox"),
    html.Div(children=[
        html.Div(children=[
            html.Label('Arm length'),
            dcc.Slider(
                id='armLensgth-slider',
                min=0,
                max=1,
                marks={
                    0: '0m',
                    1: '1m'
                },
                tooltip={"placement": "bottom", "always_visible": True},
                value=0.3,
                updatemode='drag' #Using updatemode = 'drag' you will update de plot always
            ),

            html.Br(),

            html.Label('Forearm length'),
            dcc.Slider(
                id='forearmLsength-slider',
                min=0,
                max=1,
                marks={
                    0: '0m',
                    1: '1m'
                },
                tooltip={"placement": "bottom", "always_visible": True},
                value=0.4,
                updatemode='drag' #Using updatemode = 'drag' you will update de plot always
            ),
        ], className="parameters")
    ], className="configurationBox"),

    html.Div(children=[
        html.Div(children=[
            html.Label('Arm length'),
            dcc.Slider(
                id='armLesnsgth-slider',
                min=0,
                max=1,
                marks={
                    0: '0m',
                    1: '1m'
                },
                tooltip={"placement": "bottom", "always_visible": True},
                value=0.3,
                updatemode='drag' #Using updatemode = 'drag' you will update de plot always
            ),

            html.Br(),

            html.Label('Forearm length'),
            dcc.Slider(
                id='foreardmLsength-slider',
                min=0,
                max=1,
                marks={
                    0: '0m',
                    1: '1m'
                },
                tooltip={"placement": "bottom", "always_visible": True},
                value=0.4,
                updatemode='drag' #Using updatemode = 'drag' you will update de plot always
            ),
        ], className="parameters")
    ], className="configurationBox")
], className="databarWrapper")
], className="databar")

CB_Display = html.Div(children=[
    html.H2('Display'),

    html.Div(children=[
        dcc.Checklist(['Im(J^T)'],
                    ['Im(J^T)']
        ),        
        dcc.Checklist(['Nt'],
                    ['Nt']),
        html.Hr(),
        dcc.Checklist(['Basis of Im(J^T)'],
                    []),
        dcc.Checklist(['Generators of Nt'],
                    ['Generators of Nt']),
        dcc.Checklist(['Origin'],
                    ['Origin']),
    ], className="parameters")
], className="configurationBox")

CB_Bodies = html.Div(children=[
    html.H2('Bodies'),

    html.Div(children=[
        html.Label('Arm length'),
        dcc.Slider(
            id='armLength-slider',
            min=0,
            max=1,
            marks={
                0: '0m',
                1: '1m'
            },
            tooltip={"placement": "bottom", "always_visible": True},
            value=0.3,
            updatemode='drag' #Using updatemode = 'drag' you will update de plot always
        ),

        html.Br(),

        html.Label('Forearm length'),
        dcc.Slider(
            id='forearmLength-slider',
            min=0,
            max=1,
            marks={
                0: '0m',
                1: '1m'
            },
            tooltip={"placement": "bottom", "always_visible": True},
            value=0.4,
            updatemode='drag' #Using updatemode = 'drag' you will update de plot always
        ),
    ], className="parameters")
], className="configurationBox")



def get_q_sliders():
    q_sliders = []
    
    for joint in model.getJointSet():
        num_coord = joint.numCoordinates()

        for i in range(num_coord):
            coord = joint.get_coordinates(i)
            if(coord.getLocked(state)):
                title = html.P(coord.getName() + " (locked)", style={'color': 'gray'})
                q_sliders.append(title)                
            else:
                title = html.Label(coord.getName())
                q_sliders.append(title)
            
                slider = dcc.Slider(
                    id={
                        "type": "joints-sliders",
                        "name": coord.getName()
                    },
                    min=coord.getRangeMin()*180/np.pi,
                    max=coord.getRangeMax()*180/np.pi,
                    # marks={i: str(i) for i in range(1, 90)},
                    marks={
                        -90:'-90°',
                        0: '0°',
                        90: '90°',
                        180: '180°'
                    },
                    tooltip={"placement": "bottom", "always_visible": True},
                    value=0,
                    step=0.1,
                    updatemode='drag' #Using updatemode = 'drag' you will update de plot always
                )
                q_sliders.append(slider)
            q_sliders.append(html.Br())

    return q_sliders

CB_Joints = html.Div(children=[
    html.H2('Joints'),

    html.Div(children=[
        html.Div(children=get_q_sliders()),
    ], className="parameters")
], className="configurationBox")

CB_Cables = html.Div(children=[
    html.H2('Cables'),

    html.Div(children=[
        html.P('-Manipulate the t vector here'),
        html.P('-Manipulate the cable origins and insertions')
    ], className="parameters")
], className="configurationBox")

sidebar = html.Div(children=[
    CB_Display,
    CB_Bodies,
    CB_Joints,
    CB_Cables,    
], className="sidebar")

visualizer = html.Div(children=[
    dcc.Graph(id='graphTest', mathjax=True),
], className="visualizer")

main = html.Div(children=[sidebar, visualizer], className="content")
app.layout = html.Div([header, main, databar], className="appWrapper")

def getFigJacobianTranspose():
    global first, fig
    Jt = osimModel.computeJacobianTranspose()
    p1 = Jt[:,0]
    p2 = Jt[:,1]
    p3 = Jt[:,2]

    v1 = p3 - p1
    v2 = p2 - p1

    n = np.cross(v1, v2)
    a, b, c = n
    d = np.dot(n, p3)
    print('The equation of J1 is {0}x + {1}y + {2}z = {3}'.format(a, b, c, d))

    X, Y = np.meshgrid(range(-20,20), range(-20,20))
    Z = (d - a * X - b * Y) / c

    #Color to display - Hexadecimal format (just for trying)
    yellow =  "#f9ff33"
    red =  "#ff0606"
    blue = "#0618ff"
    #If you want gradient you can just change one color of the colorscale [[0, color_1],[1, color_2]]
    figJt = go.Surface(z=Z, x=X, y=Y, opacity=.8, showscale=False, colorscale=[[0,red],[1,red]])
    fig = go.Figure()
    
    fig.add_trace(figJt)

    fig.add_scatter3d(
        x=[0], 
        y=[0], 
        z=[0], 
        mode="markers",
        marker=dict(
            size=2,          
            color="rgb(0,0,0)"))

    '''
    camera = dict(
        up=dict(x=0, y=1, z=0),
        center=dict(x=0, y=0, z=0),
        eye=dict(x=2, y=0.5, z=2)
    )
    '''

    # The parameter uirevision let us know if we want to update de view
    # if it is a constant value, it will never update. BUUT if we zoom
    # we need to rotate the camera before we update a slider -> I don't know why

    fig.update_layout(
        title=r'$\mathcal{P}_{\tau} \text{ and } Im(J^T)$',
        legend_title="Legend Title",
        scene = dict(
            xaxis_title='x',
            yaxis_title='y',
            zaxis_title='z'),
        scene_aspectmode='cube',
        uirevision = "never update the camera"
    )

    return fig


@app.callback(
    Output('graphTest', 'figure'),
    Input({'type': 'joints-sliders', 'name': ALL}, 'value')
)
def update_q(values):
    # print(ctx.args_grouping[0])

    for input in ctx.args_grouping:
        print(input.value*np.pi/180)
        print(input.id.name)    
        if(input.triggered):    
            for coord in model.getCoordinateSet():
                if coord.getName() == input.id.name:
                    coord.setValue(state, input.value*np.pi/180)

    fig = getFigJacobianTranspose()
    return fig
    # return f'Output: {sPS}, {sFE}, {eFE}'


@app.callback(
    Output('graphTest', 'figure'),
    Input('armLength-slider', 'value')
)
def update_length(values):
    # print(ctx.args_grouping[0])
    # print("here")

    fig = getFigJacobianTranspose()
    return fig
    # return f'Output: {sPS}, {sFE}, {eFE}'



if __name__ == '__main__':
    app.run_server(debug=True)
