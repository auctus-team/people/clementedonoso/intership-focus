# Intership work
Repository that includes the work done during my internship. It is separated into folders for greater ease around the identification of the topics covered.

## Subjects
### Visual Perception
A tool to understand the work done and visualize the polytope in real time on your body.

### Visualizer
The construction of a visualizer of the constituant elements of the force feasible set in opensim model and thus be able to show the work carried out and the research carried out

### Upper Limb Study
It corresponds to the study carried out on the state of the art in research (forces and elements to generate a dislocation of the shoulder), scaled model and worked tools, generation of the cone that visualizes the range of movements, among other elements.