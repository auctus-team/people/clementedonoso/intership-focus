# Upper-Limb-Study

Corresponds to the study carried out for Nasser

## About the repository
- In the ```Markers Data folder``` you will find the files that contain the markers (both the original ones, as well as those modified so that the orientation fits with the model)
- In the ```model folder``` you will find all the files regarding the model (geometry, IK files) as well as the files necessary to scale it and the results of it.
- In the ```state of the art folder``` you will find the document created from a search in the literature on the information corresponding to the dislocation
- In the ```codes folder``` you will find all the codes necessary to replicate the study carried out
- The ```Joint range of motion.pdf``` file has all the information regarding the measured values ​​of the markers, the range of motion according to the movement studied, and the weights that were modified for that measurement. To modify the file you can click [here](https://www.overleaf.com/9329382712nhtpzkshvsjq)
- To the display the markers you can use the software **[Mokka](http://biomechanical-toolkit.github.io/news/2016/10/08/mokka-new-links/)**

## Results
From the information provided by the markers, the vector is generated and in this way the cone that represents the range of movements of each extremity.

# File SHCC01
| Range Cone Arm | Range Cone foreArm | Range Cone Hand |
| --- | --- | --- |
| ![alt text](./Images/coneArm-SHCC01.png) | ![alt text](./Images/coneForearm-SHCC01.png) | XXX |

# File SCHCC02
| Range Cone Arm | Range Cone foreArm | Range Cone Hand |
| --- | --- | --- |
| ![alt text](./Images/coneArm-SHCC02.png) | ![alt text](./Images/coneForearm-SHCC02.png) | XXX |

# File SHCC03
| Range Cone Arm | Range Cone foreArm | Range Cone Hand |
| --- | --- | --- |
| ![alt text](./Images/coneArm-SHCC03.png) | ![alt text](./Images/coneForearm-SHCC03.png) | XXX |