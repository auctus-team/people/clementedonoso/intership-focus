import org.opensim.modeling.*
model = Model("model_scaled.osim");
state = model.initSystem();

motFile = ReadMotFile();
% Information columns
lenghtFile = length(motFile);
armFlexCol = motFile(:,27);
armAddCol = motFile(:,28);
armRotCol = motFile(:,29);
elbowFlexCol = motFile(:,30);
wristFlexCol = motFile(:,32);
wristDevCol = motFile(:,33);

jointSet = model.getJointSet();
plotCone("circle", jointSet, state, lenghtFile, armFlexCol, armAddCol, armRotCol, elbowFlexCol, wristFlexCol, wristDevCol);
%% Function for plot cone
function plotCone(type, jointSet, state, lenghtFile, armFlexCol, armAddCol, armRotCol, elbowFlexCol, wristFlexCol, wristDevCol)
    % INPUT     :  type: what type of cone you want to plot
    %              args: Just global variables
    % OUPUT     : None, graph of the vectors

    figure, hold on, grid on
    
    xlim([-2 2])
    ylim([-2 2])
    zlim([-2 2])

    if strcmp(type, "arm")
        title('Cone for range of arm')
        for i = 1:lenghtFile
            % Match the data
            elbowFlex = elbowFlexCol(i);
            wristFlex = wristFlexCol(i);
            wristDev = wristDevCol(i);
            armFlex = armFlexCol(i);
            armAdd = armAddCol(i);
            armRot = armRotCol(i);

            % Set position of the shoulder
            shoulder = jointSet.get(14);
            arm_flex_r = shoulder.get_coordinates(0);
            arm_flex_r.setValue(state, deg2rad(armFlex));
            arm_add_r = shoulder.get_coordinates(1);
            arm_add_r.setValue(state, deg2rad(armAdd));
            arm_rot_r = shoulder.get_coordinates(2);
            arm_rot_r.setValue(state, deg2rad(armRot));

            % Set position of the elbow
            elbow = jointSet.get(15);
            elbow_flex_r = elbow.get_coordinates(0);
            elbow_flex_r.setValue(state, deg2rad(elbowFlex));

            % Set position of the hand
            hand = jointSet.get(17);
            wrist_flex_r = hand.get_coordinates(0);
            wrist_flex_r.setValue(state, deg2rad(wristFlex));
            wrist_dev_r = hand.get_coordinates(1);
            wrist_dev_r.setValue(state, deg2rad(wristDev));

            % Obtain positions
            elbowFrame = elbow.getParentFrame();
            elbowPos = elbowFrame.getPositionInGround(state); 
            elbowPos = eval(elbowPos.toString().substring(1));
            handFrame = hand.getParentFrame();
            handPos = handFrame.getPositionInGround(state); 
            handPos = eval(handPos.toString().substring(1));

            % Plot
            arm = handPos - elbowPos;
            quiver3(0,0,0,arm(1), arm(2), arm(3));
        end
    end
    if strcmp(type, "forearm")
        title('Cone for maximun range of foreArm')

        for i = 1:lenghtFile
            armFlex = armFlexCol(i);
            armAdd = armAddCol(i);
            armRot = armRotCol(i);
            elbowFlex = elbowFlexCol(i);
            wristFlex = wristFlexCol(i);
            wristDev = wristDevCol(i);

            % Set position of the shoulder
            shoulder = jointSet.get(14);
            arm_flex_r = shoulder.get_coordinates(0);
            arm_flex_r.setValue(state, deg2rad(armFlex));
            arm_add_r = shoulder.get_coordinates(1);
            arm_add_r.setValue(state, deg2rad(armAdd));
            arm_rot_r = shoulder.get_coordinates(2);
            arm_rot_r.setValue(state, deg2rad(armRot));

            % Set position of the elbow
            elbow = jointSet.get(15);
            elbow_flex_r = elbow.get_coordinates(0);
            elbow_flex_r.setValue(state, deg2rad(elbowFlex));

            % Set position of the hand
            hand = jointSet.get(17);
            wrist_flex_r = hand.get_coordinates(0);
            wrist_flex_r.setValue(state, deg2rad(wristFlex));
            wrist_dev_r = hand.get_coordinates(1);
            wrist_dev_r.setValue(state, deg2rad(wristDev));

            % Obtain positions
            shoulderFrame = shoulder.getParentFrame();
            shoulderPos = shoulderFrame.getPositionInGround(state); 
            shoulderPos = eval(shoulderPos.toString().substring(1));
            elbowFrame = elbow.getParentFrame();
            elbowPos = elbowFrame.getPositionInGround(state); 
            elbowPos = eval(elbowPos.toString().substring(1));

            % Plot
            forearm = elbowPos - shoulderPos;
            quiver3(0,0,0,forearm(1), forearm(2), forearm(3));
        end
    end
    if strcmp(type, "circle")
        title('Circle for maximun range of arm')

        for i = 1:lenghtFile
            armFlex = armFlexCol(i);
            armAdd = armAddCol(i);
            armRot = armRotCol(i);
            elbowFlex = elbowFlexCol(i);
            wristFlex = wristFlexCol(i);
            wristDev = wristDevCol(i);

            % Set position of the shoulder
            shoulder = jointSet.get(14);
            arm_flex_r = shoulder.get_coordinates(0);
            arm_flex_r.setValue(state, deg2rad(armFlex));
            arm_add_r = shoulder.get_coordinates(1);
            arm_add_r.setValue(state, deg2rad(armAdd));
            arm_rot_r = shoulder.get_coordinates(2);
            arm_rot_r.setValue(state, deg2rad(armRot));

            % Set position of the elbow
            elbow = jointSet.get(15);
            elbow_flex_r = elbow.get_coordinates(0);
            elbow_flex_r.setValue(state, deg2rad(elbowFlex));

            % Set position of the hand
            hand = jointSet.get(17);
            wrist_flex_r = hand.get_coordinates(0);
            wrist_flex_r.setValue(state, deg2rad(wristFlex));
            wrist_dev_r = hand.get_coordinates(1);
            wrist_dev_r.setValue(state, deg2rad(wristDev));
            handFrame = hand.getParentFrame();
            handPos = handFrame.getPositionInGround(state); 
            handPos = eval(handPos.toString().substring(1));

            % Obtain positions
            shoulderFrame = shoulder.getParentFrame();
            shoulderPos = shoulderFrame.getPositionInGround(state); 
            shoulderPos = eval(shoulderPos.toString().substring(1));
            elbowFrame = elbow.getParentFrame();
            elbowPos = elbowFrame.getPositionInGround(state); 
            elbowPos = eval(elbowPos.toString().substring(1));
            
            % Plot
            plot3(handPos(1), handPos(2), handPos(3), '-r');
            
        end
    end
    view([0.5 0.5 0.5])
end

%% Function for load MOT files
function [data, labels] = ReadMotFile(fname, path)
% INPUT :  fname - the .mot file path that you wish to load
%         (leave blank to choose .mat file from a dialog box)
% OUPUT   : x : x data (Times)
%         : y : y data (Angles)
%         : Labels : Labels des data x et y (ddl names)
%
% Written by Hernandez Vincent (University of Toulon - Laboratoire HandiBio)

    if nargin < 2
        [fname, PathData] = uigetfile('*.mot', 'Ouvrir le fichier IK .mot d''OpenSim');
        fname = cellstr(fname);
        cd(PathData)
        path = [PathData, fname{1}];
        fid = fopen(path);
    else
        path = [path, fname];
        fid = fopen(path);
    end

    nlrows = 1;     nhead = 1;     buffer = fgetl(fid); 
    
    while strcmpi(strtrim(buffer), 'endheader') == 0, 
        if nhead == 1, 
            extracted.name = buffer;  %#ok<*STRNU>
        end 
        nhead = nhead + 1; 
        if ~isempty(findstr(buffer, 'nColumns')),  %#ok<*FSTR>
            % get number of columns from the header 
            ncols = strread(buffer, '%*s%d', 'delimiter', '='); 
        end 
        if ~isempty(findstr(buffer, 'nRows')), 
            % get number of rows from the header 
            nrows = strread(buffer, '%*s%d', 'delimiter', '=');  %#ok<*NASGU,*FPARK>
        end 
        buffer = fgetl(fid); 
    end 
    fclose(fid); 

    %  open file for input, include error handling
    fin = fopen(path,'r');
    if fin < 0
       error(['Could not open ',fname,' for input']);
    end

    %  Read and discard header text on line at a time
    for i=1:nhead,  buffer = fgetl(fin);  end

    %  Read titles for keeps this time
    for i= 1:nlrows
       buffer = fgetl(fin);          %  get next line as a string
       for j=1:ncols
          [next,buffer] = strtok(buffer);     %#ok<*STTOK> %  parse next column label
          Templabels{j} = next; %#ok<*AGROW>
       end
    end
    
%     % Delete 'time' label (first columns)
%     for j=2:ncols
%         D.ReadMot_labels{j-1} = Templabels{j};
%     end

    %  Read in the x-y data.  Use the vetorized fscanf function to load all
    %  numerical values into one vector.  Then reshape this vector into a
    %  matrix before copying it into the x and y matrices for return.

    data = fscanf(fin,'%f');  %  Load the numerical values into one long vector
    nd = length(data);        %  total number of data points
    nr = nd/ncols;            %  number of rows; check (next statement) to make sure
    if nr ~= round(nd/ncols)
       fprintf(1,'\ndata: nrow = %f\tncol = %d\n',nr,ncols);
       fprintf(1,'number of data points = %d does not equal nrow*ncol\n',nd);
       error('data is not rectangular')
    end

    data = reshape(data,ncols,nr)';   %  notice the transpose operator
    labels = Templabels;
end